import { createReducers, createEpics } from './helpers'

const reducer = createReducers({})

export const epic = createEpics()

export default reducer
