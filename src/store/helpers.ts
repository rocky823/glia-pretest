import { Epic, combineEpics } from 'redux-observable'
import { combineReducers } from 'redux'

export type ActionType = {
  type: string
  payload?: any
}

export type Reducer<S = any, A extends ActionType = ActionType> = (
  state: S | undefined,
  action: A,
) => S

export const createActions = <T>(
  actions: T & {
    [name: string]: (...args: any) => ActionType
  },
) => actions
export const createEpics = (...args: Epic[]) => combineEpics(...args)

export const createReducers = <T>(reducers: { [name in keyof T]: Reducer }) =>
  combineReducers(reducers)
