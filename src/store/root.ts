import { combineReducers } from 'redux'
import { combineEpics } from 'redux-observable'
import reducer, { epic } from './app'

export const rootReducer = combineReducers(reducer)
export const rootEpic = combineEpics(epic)
