import {
  createStore as reduxCreateStore,
  Middleware,
  applyMiddleware,
} from 'redux'
import { createEpicMiddleware } from 'redux-observable'
import { rootReducer, rootEpic } from './root'
import { isDev } from '@/share/env'

const epicMiddleware = createEpicMiddleware()

const logger: Middleware = store => {
  console.log('@@INIT', store.getState())

  return next => action => {
    const res = next(action)
    console.log(action, '->', store.getState())

    return res
  }
}

const createStore = (initState = {}) => {
  const store = reduxCreateStore(
    rootReducer,
    initState,
    isDev ? applyMiddleware(logger) : undefined,
  )

  epicMiddleware.run(rootEpic)
  return store
}

export default createStore
