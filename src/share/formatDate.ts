const prefix0 = (stringOfNumber: number | string): string =>
  String(stringOfNumber).padStart(2, '0')

const formatDate = (formatStr = 'YYYY-MM-DDThh:mm:ss') => {
  const D = new Date()

  // D.setTime(+D + tz * 60 * 60 * 1000)

  const year = D.getFullYear()
  const month = D.getMonth()
  const date = D.getDate()
  const hours = D.getHours()
  const minutes = D.getMinutes()
  const seconds = D.getSeconds()

  /**
   *
   * @param str - Format date string. default: `YYYY-MM-DDThh:mm:ss`
   */
  const format = (str: string) =>
    str
      .replace(/[yY]+/, Y => String(year).slice(0 - Y.length))
      .replace(/M+/, MM => {
        const mm = month + 1
        return MM.length > 1 ? prefix0(mm) : String(mm)
      })
      .replace(/D+/, DD => (DD.length > 1 ? prefix0(date) : String(date)))
      .replace(/h+/, h => (h.length > 1 ? prefix0(hours) : String(hours)))
      .replace(/m+/, m => (m.length > 1 ? prefix0(minutes) : String(minutes)))
      .replace(/s+/, s => (s.length > 1 ? prefix0(seconds) : String(seconds)))

  return format(formatStr)
}

export default formatDate
