const switchCase = (type: string) => (...conditionArgs: any[]) => {
  const defaultState: any = conditionArgs.slice(-1)[0]

  const conditions: Array<Array<string | any>> = conditionArgs.slice(
    0,
    conditionArgs.length - 1,
  )

  const matched = conditions.find(condition => {
    const types = condition.slice(0, condition.length - 1)
    const typeSet = new Set(types)

    return typeSet.has(type)
  })

  if (matched) {
    const matchedAny = matched.slice(-1)[0]
    if (typeof matchedAny === 'function') return matchedAny()
    return matchedAny
  }

  return defaultState
}

export default switchCase
