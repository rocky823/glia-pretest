import React, { StatelessComponent } from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import App from './App'
import createStore from './store'

const store = createStore({})

const app = document.createElement('div')
app.id = 'app'
document.body.appendChild(app)

render(
  <Provider store={store}>
    <BrowserRouter>
      <>
        <Switch>
          <Route component={App} />
        </Switch>
      </>
    </BrowserRouter>
  </Provider>,
  app,
)
