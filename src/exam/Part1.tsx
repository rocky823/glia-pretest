import React from 'react'
import styled, { css } from 'styled-components'
import formatDate from '@/share/formatDate'

const Div = styled.div`
  ${(_p: {}) => css``}
`

const initState = {
  now: Date.now(),
}

type Props = {}
type State = Readonly<typeof initState>

class Part1 extends React.PureComponent<Props, State> {
  private t = 0
  static defaultProps = {}

  onStart = () => {
    this.timer()
  }

  onPause = () => {
    this.clearTimer()
  }

  timer = () => {
    this.setState({
      now: Date.now(),
    })

    this.t = window.setTimeout(this.timer, 1000)
  }

  clearTimer = () => {
    window.clearTimeout(this.t)
  }

  state = initState

  componentDidMount() {
    this.timer()
  }

  componentWillUnmount() {
    this.clearTimer()
  }

  render() {
    return (
      <Div>
        <h1>Part 1 - Timer </h1>
        {formatDate('hh:mm:ss')}
        <button onClick={this.timer}>Start</button>
        <button onClick={this.clearTimer}>Pause</button>
      </Div>
    )
  }
}
export default Part1
