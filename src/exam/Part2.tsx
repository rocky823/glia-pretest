import React from 'react'
import styled, { css } from 'styled-components'

const Div = styled.div`
  ${(_p: {}) => css``}
`

const initState = {}
type Props = {}
type State = Readonly<typeof initState>

class Part2 extends React.PureComponent<Props, State> {
  static defaultProps = {}

  state = initState
  componentDidMount() {}
  render() {
    const { children } = this.props

    return (
      <Div>
        <h1>Coffee RESTful API Design</h1>
      </Div>
    )
  }
}
export default Part2
