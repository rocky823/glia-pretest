import React from 'react'
import styled, { css } from 'styled-components'

import axios from 'axios'

const Div = styled.div`
  ${(_p: {}) => css``}
`

type DataJson = {
  SiteName: string
  County: string
  AQI: string
  Pollutant: string
  Status: string
  SO2: string
  CO: string
  CO_8hr: string
  O3: string
  O3_8hr: string
  PM10: string
  'PM2.5': number
  NO2: string
  NOx: string
  NO: string
  WindSpeed: string
  WindDirec: string
  PublishTime: string
  'PM2.5_AVG': number
  PM10_AVG: string
  SO2_AVG: string
  Longitude: string
  Latitude: string

  [key: string]: any
}

const Table = styled.table`
  ${(_p: {}) => css`
    border-collapse: collapse;
    width: 600px;
    tbody > tr:nth-child(even) {
      background: #eee;
    }

    th {
      cursor: pointer;
    }
    td {
      padding: 4px;
      text-align: center;
    }
  `}
`

const initState = {
  data: [],

  sortKey: {
    Site: 1,
    Pollutant: 1,
    Status: 1,
    'PM2.5_AVG': 1,
  },
}
type Props = {}
type State = Readonly<typeof initState>

class Part2 extends React.PureComponent<Props, State> {
  static defaultProps = {}

  state = initState
  componentDidMount() {
    this.fetchJSON()
  }

  fetchJSON = async () => {
    console.log('fetching data')
    const { data } = await axios({
      url: 'http://opendata2.epa.gov.tw/AQI.json',
    })

    this.setState({ data })
    setTimeout(() => {
      this.fetchJSON()
    }, 10 * 1e3)
  }

  sort = (e: React.SyntheticEvent<HTMLTableHeaderCellElement>) => {
    const { currentTarget } = e
    const { sort } = currentTarget.dataset

    const key = sort as keyof typeof initState.sortKey

    this.setState(({ data, sortKey }) => {
      const d = data as DataJson[]
      return {
        sortKey: {
          ...sortKey,
          [key]: sortKey[key] ? 0 : 1,
        },
        data: [...d].sort((a, b) => {
          if (key === 'PM2.5_AVG') {
            return sortKey[key] ? b[key] - a[key] : a[key] - b[key]
          }

          return sortKey[key]
            ? b[key].localeCompare(a[key])
            : a[key].localeCompare(b[key])
        }) as any,
      }
    })
    // console.log(sort)
  }

  render() {
    const { children } = this.props
    const { data } = this.state
    const d = data as DataJson[]

    return (
      <Div>
        <h1>Part 1 - 2. Ajax</h1>
        <Table>
          <thead>
            <th onClick={this.sort} data-sort={'SiteName'}>
              SiteName [sort]
            </th>
            <th onClick={this.sort} data-sort={'Pollutant'}>
              Pollutant [sort]
            </th>
            <th onClick={this.sort} data-sort={'Status'}>
              Status [sort]
            </th>
            <th onClick={this.sort} data-sort={'PM2.5_AVG'}>
              PM2.5_AVG [sort]
            </th>
          </thead>
          <tbody>
            {d.map(({ SiteName, Pollutant, Status, 'PM2.5_AVG': PM2 }) => {
              return (
                <tr key={SiteName}>
                  <td>{SiteName}</td>
                  <td>{Pollutant}</td>
                  <td>{Status}</td>
                  <td>{PM2}</td>
                </tr>
              )
            })}
          </tbody>
        </Table>
      </Div>
    )
  }
}
export default Part2
