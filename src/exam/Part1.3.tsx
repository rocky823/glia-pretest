import React from 'react'
import styled, { css } from 'styled-components'

const Div = styled.div`
  ${(_p: {}) => css``}
`

const initState = {}
type Props = {}
type State = Readonly<typeof initState>

class Part3 extends React.PureComponent<Props, State> {
  static defaultProps = {}

  state = initState
  componentDidMount() {}
  render() {
    const { children } = this.props

    return (
      <Div>
        <h1>Part 1 - 3.Multiples of 3 and 5.</h1>

        <pre>
          {`
const sumSequence = (value, x) =>{
  return (Math.floor((value - 1) / x) * (x + ((Math.floor(value - 1) / x) * x))) / 2);
}

const sum_of_all_mul_3_5 = () => {
    var value=parseInt(document.getElementById("input").value);
    var result=sumSequence(value, 3) + sumSequence(value, 5) - sumSequence(value, 15);
    document.getElementById("output").value=result ;
    }
          `}
        </pre>
      </Div>
    )
  }
}
export default Part3
