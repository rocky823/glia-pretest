import React from 'react'
import styled, { css } from 'styled-components'
import { hot } from 'react-hot-loader'
import Part1 from './exam/Part1'
import Part12 from './exam/Part1.2'
import Part13 from './exam/Part1.3'
import Part2 from './exam/Part2'

const Div = styled.div`
  ${(_p: {}) => css``}
`

const initState = {}
type Props = {}
type State = Readonly<typeof initState>

@hot(module)
class App extends React.Component<Props, State> {
  static defaultProps = {}

  state = initState
  componentDidMount() {}
  render() {
    const { children } = this.props

    return (
      <Div>
        <Part1 />
        <Part12 />
        <Part13 />
        <Part2 />
      </Div>
    )
  }
}

export default App
