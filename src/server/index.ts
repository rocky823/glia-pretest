import Koa from 'koa'
import Router from 'koa-router'
import db from './db'
const app = new Koa()
const router = new Router()

router.get('/:id', (ctx, next) => {
  const { id } = ctx.params
  ctx.body = db.get('coffees')

  next()
})
// .get('/order/list', (ctx, next) => {})
// .post('/order/:id', (ctx, next) => {})

app
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(3000, () => {
    console.log(`app is listening on: 3000`)
  })

// app.listen(3000)
