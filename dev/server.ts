import chokidar, { WatchOptions } from 'chokidar'
import cluster from 'cluster'
import c from 'chalk'

import { fork, ChildProcess } from 'child_process'

const options = {
  watchDir: 'src/server',
  watchOptions: {} as WatchOptions,
}

const log = (tStrings: TemplateStringsArray, ...vars: any): void => {
  const logs = tStrings.reduce((s, t, i) => `${s}${t}${vars[i] || ''}`, '')
  console.log(`${c.grey(`[dev-server]`)} ${logs}`)
}

const forkApp: Function & {
  app?: ChildProcess
} = (file?: string) => {
  if (forkApp.app) {
    forkApp.app.kill('SIGINT')
  }

  if (file) {
    log`${c.cyan(file)} is changed.`
  }

  forkApp.app = fork('src/server', ['-r', 'ts-node/register'])
}

chokidar
  .watch('src/server', {})
  .on('change', file => forkApp(file))
  .on('ready', () => {
    log`Ready to watch:\n${JSON.stringify(options, null, 2)}`
    forkApp()
  })
