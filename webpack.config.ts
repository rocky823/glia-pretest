import webpack, { Configuration, DefinePlugin } from 'webpack'
import ForkTsChecker from 'fork-ts-checker-webpack-plugin'
import { Configuration as DevServerConf } from 'webpack-dev-server'
import { resolve } from 'path'
import HtmlPlugin from 'html-webpack-plugin'
import styledTransformer from 'typescript-plugin-styled-components'

import tsConfig from './tsconfig.json'

const { NODE_ENV, CIRCLE_SHA1 } = process.env

const isDev = !NODE_ENV || NODE_ENV === 'development'

interface Conf extends Configuration {
  devServer: DevServerConf
}

const conf: Conf = {
  mode: isDev ? 'development' : 'production',
  entry: {
    app: './src',
  },
  devtool: isDev && '#@cheap-module-eval-source-map',
  devServer: {
    host: '0.0.0.0',
    overlay: {
      warnings: true,
      errors: true,
    },
    stats: {
      modules: false,
      errors: true,
      warnings: true,
    },
    // contentBase: resolve(dist),
    // historyApiFallback: true,
  },

  output: {
    publicPath: '/',
    path: resolve('out'),
    filename: isDev ? '[name].js' : '[name]_[hash].js',
    chunkFilename: isDev ? '[name].js' : '[name]_[chunkhash].js',
  },

  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    alias: Object.entries(tsConfig.compilerOptions.paths).reduce(
      (o, [a, p]) =>
        Object.assign(o, {
          [a.replace(/..$/, '')]: resolve(p[0].replace(/..$/, '')),
        }),
      {},
    ),
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          transpileOnly: true,
          experimentalFileCaching: true,
          getCustomTransformers: () => ({
            before: [
              styledTransformer({
                getDisplayName: (filename, displayName) =>
                  `${displayName}_${filename
                    .replace(__dirname, '')
                    .replace(/\.tsx?$/, '')
                    .replace(/\//g, '_')}`,
              }),
            ],
          }),
          ...(isDev
            ? {}
            : {
                getCustomTransformers: undefined,
              }),
        },
      },
    ],
  },

  plugins: [
    new ForkTsChecker({
      async: false,
    }),
    new DefinePlugin({
      'process.env': Object.entries({
        NODE_ENV,
        IMAGE_TAG: CIRCLE_SHA1,
        BUILD_TIME: new Date().toString(),
        REACT_VER: require('react').version,
        IS_DEV: isDev,
      }).reduce(
        (o, [k, v]) => Object.assign(o, { [k]: JSON.stringify(v) }),
        {},
      ),
    }),

    new HtmlPlugin({
      // template: (() => {
      //   const index = `${dist}/index.html`
      //   toHTML(index)
      //   return index
      // })(),
      minify: {
        minifyCSS: !isDev,
        collapseWhitespace: !isDev,
      },
    }) as any,
  ],
}
export default conf
